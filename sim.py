import json
from os import path

import numpy as np

import hoomd
import hoomd.md

if not path.exists("init.gsd"):
    with hoomd.context.initialize(""):
        N = 9
        snap = hoomd.data.make_snapshot(
            N=N,
            box=hoomd.data.boxdim(Lx=N * 3, Ly=3, Lz=3),
            particle_types=["A", "B", "C"],
        )

        # Set x values
        snap.particles.position[:, 0] = np.array([_ * 3 for _ in range(9)]) - (
            N * 3 / 2
        )

        snap.particles.typeid[:] = [0] * 1 + [1] * 2 + [2] * 6

        snap.bonds.resize(6)
        snap.bonds.types = ["B-B", "C-C"]
        snap.bonds.group[:] = [[1, 2], [3, 4], [4, 5], [5, 6], [6, 7], [7, 8]]
        snap.bonds.typeid[:] = [0] + [1] * 5

        snap.angles.resize(4)
        snap.angles.types = ["C-C-C"]
        snap.angles.group[:] = [[3, 4, 5], [4, 5, 6], [5, 6, 7], [6, 7, 8]]

        snap.dihedrals.resize(3)
        snap.dihedrals.types = ["C-C-C-C"]
        snap.dihedrals.group[:] = [[3, 4, 5, 6], [4, 5, 6, 7], [5, 6, 7, 8]]
        snap.replicate(1, 9, 9)
        hoomd.init.read_snapshot(snap)
        hoomd.dump.gsd(
            filename="init.gsd", overwrite=True, period=None, group=hoomd.group.all()
        )
my_context = hoomd.context.initialize("")
system = hoomd.init.read_gsd("init.gsd", restart="restart.gsd")

nl = hoomd.md.nlist.cell()
lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)
lj.set_params(mode="xplor")
lj.pair_coeff.set(["A", "B", "C"], ["A", "B", "C"], epsilon=1.0, sigma=1.0)

harmonic_bond = hoomd.md.bond.harmonic()
harmonic_bond.bond_coeff.set("B-B", k=100, r0=1)
harmonic_bond.bond_coeff.set("C-C", k=100, r0=1)

harmonic_angle = hoomd.md.angle.harmonic()
harmonic_angle.angle_coeff.set("C-C-C", k=10.0, t0=np.pi / 4)

harmonic_dihedral = hoomd.md.dihedral.harmonic()
harmonic_dihedral.dihedral_coeff.set("C-C-C-C", k=10.0, d=1, n=1)
start_dt = 1
integrator = hoomd.md.integrate.mode_standard(dt=start_dt)
all_particles = hoomd.group.all()
langevin = hoomd.md.integrate.langevin(group=all_particles, kT=10, seed=0)

gsd_restart = hoomd.dump.gsd(
    filename="restart.gsd", group=all_particles, truncate=True, period=1e3, phase=0
)

# Find a dt that works
tuned = False
dt = start_dt
tune_time = 1e4
# Check to see if we are tuned
if path.exists("metadata.json"):
    with open("metadata.json") as json_file:
        metadata = json.load(json_file)
        # Read in the old dt, then skip out of tuneing
        dt = metadata["hoomd.md.integrate.mode_standard"]["dt"]
        integrator.set_params(dt=dt)
        # Skip tuneing
        tuned = True

while not tuned:
    try:
        print(f"dt: {dt}")
        print(f"current timestep {system.get_metadata()['timestep']}")
        temp_snap = system.take_snapshot(all=True)
        hoomd.run_upto(tune_time)
        tuned = True
    except RuntimeError:
        system.restore_snapshot(temp_snap)
        dt /= 2
        integrator.set_params(dt=dt)

hoomd.dump.gsd(
    filename="traj.gsd", overwrite=False, period=1e3, group=all_particles, phase=0
)

# Shrink to a smaller density
shrink_time = 1e4 + tune_time
N_particles = len(system.particles)
number_density = 1
target_lenght = (N_particles / number_density) ** (1 / 3)
size_variant = hoomd.variant.linear_interp(
    [(tune_time, system.box.Lx), (shrink_time, target_lenght)], zero=0
)
box_resize = hoomd.update.box_resize(L=size_variant)
hoomd.run_upto(shrink_time)
box_resize.disable()

langevin.set_params(kT=1.0)
hoomd.run_upto(2e5)
gsd_restart.write_restart()
hoomd.meta.dump_metadata(filename="metadata.json")
print("done")
