import mbuild as mb


class A(mb.Compound):
    def __init__(self):
        super(A, self).__init__()
        self.mass = 1

        _ = mb.Particle(pos=[0.0, 0.0, 0.0], name='A')
        self.add([_])


class B(mb.Compound):
    def __init__(self):
        super(B, self).__init__()
        self.mass = 1

        B1 = mb.Particle(pos=[0.0, 0.0, 0.0], name='B')
        B2 = mb.Particle(pos=[1.0, 0.0, 0.0], name='B')
        self.add([B1, B2])
        self.add_bond((B1, B2))


class C(mb.Compound):
    def __init__(self):
        super(C, self).__init__()
        self.mass = 1

        C1 = mb.Particle(pos=[0.0, 0.0, 0.0], name='C')
        C2 = mb.Particle(pos=[1.0, 0.0, 0.0], name='C')
        C3 = mb.Particle(pos=[2.0, 0.0, 0.0], name='C')
        C4 = mb.Particle(pos=[3.0, 0.0, 0.0], name='C')
        C5 = mb.Particle(pos=[4.0, 0.0, 0.0], name='C')
        C6 = mb.Particle(pos=[5.0, 0.0, 0.0], name='C')

        self.add([C1, C2, C3, C4, C5, C6])
        self.add_bond((C1, C2))
        self.add_bond((C2, C3))
        self.add_bond((C3, C4))
        self.add_bond((C4, C5))
        self.add_bond((C5, C6))
